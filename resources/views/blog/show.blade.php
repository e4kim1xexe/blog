@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{route('users.show', ['user' => $blog->user->id])}}">
                {{$blog->user->name}}
            </a>
        </div>
        <div class="card-body">
            <h5 class="card-title">{{$blog->title}}</h5>
            <p class="card-text">{{$blog->body}}</p>
        </div>
        <div class="card-footer text-muted">
            Дата: {{$blog->getDateTime()}}
        </div>
    </div>
    @canany('deleteOrUpdate', $blog)
        <a href="{{route('blogs.edit', ['blog' => $blog->id])}}" class="btn btn-success btn-lg edit mt-2"
           type="button"> {{__('Редактировать')}}</a>
        <button type="button" class="btn btn-primary edit delete mt-2" data-bs-toggle="modal"
                data-bs-target="#exampleModal">
            Удалить
        </button>
    @endcanany
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('Подтверждение')}}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p> {{__('Вы уверены что хотите удалить пост?')}} </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('Закрыть')}}</button>
                    <form action="{{route('blogs.destroy', ['blog' => $blog->id])}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-secondary" >{{__('Удалить')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
