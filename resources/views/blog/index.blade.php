@extends('layouts.app')
@section('content')
    @guest()
        <p class="registr">{{__('Войдите в систему для того что бы добавить пост!')}}</p>
    @endguest
    <a class="btn btn-success mb-2" href="{{route('blogs.create')}}">{{__('Добавить пост')}}</a>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($blogs as $blog)
            <div class="col">
                <div class="card h-100">
                    <div class="card-footer">
                        <small class="text-muted">{{$blog->title}}</small>
                    </div>
                    <a href="{{route('blogs.show', ['blog' => $blog->id])}}" class="card-body text-decoration-none">
                        <h5 class="card-title">{{$blog->title}}</h5>
                        <p class="card-text">{{$blog->body}}</p>
                    </a>
                    <div class="card-footer d-flex flex-column">
                        <small class="text-muted">{{__('Дата:')}} {{$blog->getDateTime()}}</small>
                        <small class="text-muted">{{__('Автор:')}} <a
                                href="{{route('users.show', ['user' => $blog->user->id])}}">{{$blog->user->name}}</a></small>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="justify-content-center pb-3 mt-3">
        <ul class="pagination-list">
            {{ $blogs->links() }}
        </ul>
    </div>
@endsection
