@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{route('blogs.store')}}">
                @csrf
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="mb-3">
                    <label for="title" class="form-label">{{__('Заголовок')}}</label>
                    <input type="text" name="title" class="form-control" id="title"
                           aria-describedby="title" value="{{ old('title') }}">
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="content" class="form-label">{{__('Блог')}}</label>
                    <textarea type="text" name="body" class="form-control" id="content"
                              aria-describedby="content">{{old('body')}}</textarea>
                    @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">{{__('Добавить')}}</button>
            </form>
        </div>
    </div>
@endsection
