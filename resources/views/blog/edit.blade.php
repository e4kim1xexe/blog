@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{route('blogs.update', ['blog' => $blog])}}">
                @method('PUT')
                @csrf
                <input type="hidden" name="user_id" value="{{$blog->user->id}}">
                <div class="mb-3">
                    <label for="title" class="form-label">{{__('Заголовок')}}</label>
                    <input type="text" name="title" class="form-control" id="title"
                           aria-describedby="title" VALUE="{{$blog->title}}">
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="content" class="form-label">{{__('Блог')}}</label>
                    <textarea type="text" name="body" class="form-control" id="content"
                              aria-describedby="content">{{$blog->body}}</textarea>
                    @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">{{__('Сохранить')}}</button>
            </form>
        </div>
    </div>


@endsection
