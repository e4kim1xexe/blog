@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ __('Перейдите на свою почту для подтверждения!') }}
                            </div>
                        @endif
                        {{ __('Перейдите на свою почту для подтверждения!') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
