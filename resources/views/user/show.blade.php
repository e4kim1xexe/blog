@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            {{__('Данные пользователя')}}
        </div>
        <div class="card-body pt-4">
            <blockquote class="blockquote mb-0">
                <footer class="blockquote-footer">Имя: <cite title="Source Title">{{$user->name}}</cite></footer>
                <footer class="blockquote-footer">Почта: <cite title="Source Title">{{$user->email}}</cite></footer>
            </blockquote>
        </div>
    </div>
    @if(count($blogs) != 0)
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="text-center">{{__('Посты пользователя')}}</h3>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-3 g-4 justify-content-around">
            @foreach($blogs as $blog)
                <div class="col">
                    <div class="card h-100">
                        <div class="card-footer">
                            <small class="text-muted">{{$blog->title}}</small>
                        </div>
                        <a href="{{route('blogs.show', ['blog' => $blog->id])}}" class="card-body text-decoration-none">
                            <h5 class="card-title">{{$blog->title}}</h5>
                            <p class="card-text">{{$blog->body}}</p>
                        </a>
                        <div class="card-footer d-flex flex-column">
                            <small class="text-muted">{{__('Дата:')}} {{$blog->getDateTime()}}</small>
                            <small class="text-muted">{{__('Автор:')}} <a
                                    href="{{route('users.show', ['user' => $blog->user->id])}}">{{$blog->user->name}}</a></small>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
    <div class="justify-content-center pb-3 mt-3">
        <ul class="pagination-list">
            {{ $blogs->links() }}
        </ul>
    </div>

@endsection
