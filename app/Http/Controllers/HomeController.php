<?php

namespace App\Http\Controllers;

use App\Notifications\SuccessRegisterNotification;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\View as ViewType;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Renderable
     */
    public function index(): Renderable
    {
        $user = Auth::user();
        $user->notify(new SuccessRegisterNotification($user));
        return view('home');
    }

    /**
     * @return ViewType
     */
    public function confirmation(): ViewType
    {
        return view('confirmation');
    }
}
