<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\View\View as ViewType;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('show');
    }

    /**
     * @param User $user
     * @return ViewType
     */
    public function show(User $user): ViewType
    {
        $blogs = $user->blogs()->paginate(6);
        return view('user.show', compact('user', 'blogs'));
    }
}
