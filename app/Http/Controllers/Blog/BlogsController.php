<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogCreateRequest;
use App\Http\Requests\BlogEditRequest;
use App\Models\Blog;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View as ViewType;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class BlogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * @return ViewType
     */
    public function index(): ViewType
    {
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(12);
        return view('blog.index', compact('blogs'));
    }

    /**
     * @return ViewType
     */
    public function create(): ViewType
    {
        $user = Auth::user();
        return view('blog.create', compact('user'));
    }


    /**
     * @param BlogCreateRequest $request
     * @return RedirectResponse
     */
    public function store(BlogCreateRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $blog = Blog::create($data);
        return redirect()->route('blogs.show', ['blog' => $blog])->with('status', __("Блог успешно был создан!"));
    }


    /**
     * @param Blog $blog
     * @return ViewType
     */
    public function show(Blog $blog): ViewType
    {
        return view('blog.show', compact('blog'));
    }


    /**
     * @param Blog $blog
     * @return ViewType
     * @throws AuthorizationException
     */
    public function edit(Blog $blog): ViewType
    {
        $this->authorize('deleteOrUpdate', $blog);
        return view('blog.edit', compact('blog'));
    }


    /**
     * @param BlogEditRequest $request
     * @param Blog $blog
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(BlogEditRequest $request, Blog $blog): RedirectResponse
    {
        $this->authorize('deleteOrUpdate', $blog);
        $data = $request->validated();
        $blog->update($data);
        return redirect()->route('blogs.show', ['blog' => $blog])->with('status', __("Блог успешно был обновлен!"));
    }


    /**
     * @param Blog $blog
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Blog $blog): RedirectResponse
    {
        $this->authorize('deleteOrUpdate', $blog);
        $blog->delete();
        return redirect()->route('blogs.index')->with('status', __("Блог успешно был удален!"));
    }
}
