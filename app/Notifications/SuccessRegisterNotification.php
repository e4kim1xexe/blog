<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SuccessRegisterNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->line(__('Уважаемый, ') . $this->user->name . __(' Поздравляем вас с успешной регистрацией на нашем сайте!'))
            ->action('Перейти на сайт', url('http://127.0.0.1:8000/users/' . $this->user->id));
    }

}
