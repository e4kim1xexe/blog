<?php

namespace App\Policies;

use App\Models\Blog;
use App\Models\User;

class BlogPolicy
{
    /**
     * @param User $user
     * @param Blog $blog
     * @return bool
     */
    public function deleteOrUpdate(User $user, Blog $blog): bool
    {
        return $user->id === $blog->user_id;
    }
}
