<?php

use App\Http\Controllers\Blog\BlogsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\User\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [BlogsController::class, 'index'])->name('home');
Route::get('/users/{user}', [UsersController::class, 'show'])->name('users.show');
Route::resource('blogs', BlogsController::class);

Auth::routes();
Auth::routes(['verify' => true]);
Route::get('/verify', [HomeController::class, 'confirmation'])->name('confirmation');

Route::get('/home', [HomeController::class, 'index'])->name('home');
