<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Blog;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create([
            'email' => 'test@test.com',
            'password' => Hash::make('test'),
        ]);
        $users = User::factory()->count(10)->create();
        foreach ($users as $user) {
            Blog::factory()->count(6)->create(['user_id' => $user->id]);
        }
    }
}
